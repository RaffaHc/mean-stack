var gulp = require('gulp'),
    browserify = require('browserify'),
    source = require('vinyl-source-stream'),
    buffer = require('vinyl-buffer'),
    uglify = require('gulp-uglify'),
    sourcemaps = require('gulp-sourcemaps'),
    config = require('../config/_browserify'),
    paths = require('./_paths');

function browseDependencies() {
    var configDependencies = config;
    configDependencies.development.entries = paths.appResources + '_dependecies.js';

    browserify(configDependencies.development).bundle()
        .pipe(source('dependecies.js'))
        .pipe(buffer())
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(uglify())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(paths.webResources));

    console.info('\nDependecies.js Build Completed!\n');
}

function browseApplication() {
    var configApplication = config;
    configApplication.development.entries = paths.appResources + '_application.js';

    browserify(configApplication.development).bundle()
        .pipe(source('application.js'))
        .pipe(buffer())
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(uglify())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(paths.webResources));

    console.info('\nApplication.js Build Completed!\n');
}

function browserifyDev() {
    browseDependencies();
    browseApplication();
}

gulp.task('browserify:dependencies', browseDependencies);
gulp.task('browserify:application', browseApplication);
gulp.task('browserify:dev', browserifyDev);
