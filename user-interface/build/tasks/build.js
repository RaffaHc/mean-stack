var gulp = require('gulp'),
    runSequence = require('run-sequence');

function buildDev() {
    runSequence (
        ['browserify:dev', 'sass:dev', 'template:dev'],
        ['watch:sass', 'watch:application', 'watch:dependencies', 'watch:resources', 'watch:template']
    );
}

gulp.task('build:dev', buildDev);
