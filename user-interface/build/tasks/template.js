var gulp = require('gulp'),
//    minifyHtml = require('gulp-minify-html'),
    ngTemplate = require('gulp-ng-template'),
    config = require('../config/_template'),
    paths = require('./_paths');

function templateDev() {
    gulp.src(paths.appPackage + '**/*.html')
//    .pipe(minifyHtml({empty: true, quotes: true}))
    .pipe(ngTemplate(config.development))
    .pipe(gulp.dest(paths.webResources));

    console.info('\Templates.js Build Completed!\n');
}

gulp.task('template:dev', templateDev);
