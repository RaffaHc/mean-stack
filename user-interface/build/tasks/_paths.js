// paths basead on gulpfile location
var paths = {
    appPackage: 'app/',
    appResources: 'resources/',
    webPackage: '../web-package/',
    webResources: '../web-package/resources',

};

module.exports = paths;
