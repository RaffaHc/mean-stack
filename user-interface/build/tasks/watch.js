var gulp = require('gulp'),
    paths = require('./_paths');

function watchSass() {
    gulp.watch(paths.appPackage + '**/*.scss', ['sass:dev']);
    gulp.watch(paths.appResources + '**/*.scss', ['sass:dev']);
}

function watchResources() {
    gulp.watch(paths.appResources + '_application.js', ['browserify:application']);
}

function watchDependencies() {
    gulp.watch(paths.appResources + '_dependecies.js', ['browserify:dependencies']);
}

function watchApplication() {
    gulp.watch(paths.appPackage + '**/*.js', ['browserify:application']);
}

function watchTemplate() {
    gulp.watch(paths.appPackage + '**/*.html', ['template:dev']);
}

gulp.task('watch:sass', watchSass);
gulp.task('watch:resources', watchResources);
gulp.task('watch:dependencies', watchDependencies);
gulp.task('watch:application', watchApplication);
gulp.task('watch:template', watchTemplate);

