var gulp = require('gulp'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    config = require('../config/_sass'),
    paths = require('./_paths');

/* FUNCTIONS */
function sassDev() {
    gulp.src(paths.appResources + '**/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass(config.development).on('error', sass.logError))
        .pipe(sourcemaps.write(/* paths.webResources */))
        .pipe(gulp.dest(paths.webResources));

    console.info('\nSASS Build Completed!\n');
}

gulp.task('sass:dev', sassDev);
