var config = {
    development: {
        debug: true,
        fullPaths: true
    }
};

module.exports = config;
