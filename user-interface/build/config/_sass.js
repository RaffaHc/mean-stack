var includePaths = [
        'node_modules/bootstrap-sass/assets/stylesheets/',
        'app'
    ],

    config = {
        development: {
            includePaths: includePaths
        }
    };

module.exports = config;
