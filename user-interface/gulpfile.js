var gulp = require('gulp'),
    requireDir = require('require-dir');

requireDir('./build/tasks', { recurse: true});

gulp.task('default', ['build:dev']);
