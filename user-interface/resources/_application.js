// MAIN MODUlE
require('../app/_board-module');

// ROOT
require('../app/root/_root-module');
require('../app/root/root');

// CONFIG
require('../app/config/_config-module');

// SECTIONS
require('../app/sections/_setion-module');

// COMPONENTS
require('../app/components/_components-module');
