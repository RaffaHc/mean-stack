(function() {

    function rootController() {

    }

    angular.module('_Root').component('root', {
        templateUrl: 'root/root.html',
        controller: rootController,
        bindings: {
            text: '@'
        }
    });

} ());
