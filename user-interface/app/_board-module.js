// Application Main Module
angular.module('_Board', [
    '_Config',
    '_Root',
    '_Sections',
    '_Components',
    '_Templates'
]);

angular.module('_Templates', []);
