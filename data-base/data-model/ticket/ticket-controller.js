var Model = require('mongoose').model('Ticket'),
    ResponseHandler = require('../../middlewares/response-handler/response-handler');

function create(req, res, next) {
    var ticket = new Model(req.body);

    ticket.save(function (err) {
        ResponseHandler.parse(ticket, res, next, err);
    });
}

function list(req, res, next) {
    Model.find({}, function (err, list){
        ResponseHandler.parse(list, res, next, err);
    });
}

function read(req, res) {
    res.json(req.model);
}

function listById(req, res, next, id) {
    Model.findOne({
        _id: id
    }, function (err, model) {
        if(err) {
            return next(err);
        } else {
            req.model = model;
            next();
        }
    });
}

exports.create = create;
exports.list = list;
exports.read = read;
exports.listById = listById;
