var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var Ticket = new Schema({
    code: String,
    title: String,
    desciption: String,
    status: String,
    priority: String,
});

mongoose.model('Ticket', Ticket);
