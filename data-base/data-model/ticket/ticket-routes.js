var Controller = require('./ticket-controller'),
    routerConfig = require('../../middlewares/router-config/router-config');

var BASE_PATH = routerConfig.build('/tickets');

function router(app) {
    app.route(BASE_PATH)
        .post(Controller.create)
        .get(Controller.list);

    app.route(BASE_PATH + '/:id')
        .get(Controller.read);

    app.param('id', Controller.listById);
}

module.exports = router;
