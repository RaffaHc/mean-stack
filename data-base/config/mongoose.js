var mongoose  = require('mongoose'),
    dbModels = require('../data-model/data-models'),
    dbRoutes = require('../data-model/data-routes'),
    environment = require('./environments/' + process.env.NODE_ENV + '.js');

function init() {
    var db = mongoose.connect(environment.mongoose.db);
    dbModels();

    return db;
}

module.exports = {
    init: init,
    dbRoutes: dbRoutes
};
